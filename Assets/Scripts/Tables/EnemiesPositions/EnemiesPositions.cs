﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace EnemiesPositionsSetter
{
    [CreateAssetMenu(menuName = "Scriptable/EnemiesPositions", fileName = "EnemiesPositions")]
    public class EnemiesPositions : ScriptableObject
    {
        public List<EnemyPosition> Positions;
    }

}