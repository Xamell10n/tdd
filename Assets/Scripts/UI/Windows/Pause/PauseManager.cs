﻿using States;
using WindowSystem.Managers;
using SceneManager = Managers.SceneManager;

namespace CustomGUI.Windows
{
    public class PauseManager : AbstractWindowManager<PauseView>
    {
        private readonly WindowsManager _windowsManager;
        
        public PauseManager
        (
            PauseView view,
            WindowsManager windowsManager
        ) : base(view)
        {
            _windowsManager = windowsManager;
        }

        public override void Listen()
        {
            StatesManager.ChangeState(GameState.Menu);
            View.Listen(OnContinue, OnRestart, OnExit);
        }

        private void OnContinue()
        {
            _windowsManager.CloseWindow();
            StatesManager.ChangeState(GameState.Game);
        }

        private void OnRestart()
        {
            SceneManager.Load(1);
        }

        private void OnExit()
        {
            SceneManager.Load(0);
        }
    }
}