﻿using System.Text;
using Constants;
using ObjectsPool;
using Tables.Ships;
using UnityEngine;
using Zenject;

namespace Managers.Spawner
{
    public class PlayerSpawner : ShipSpawner
    {
        private readonly ShipsParameters _shipsParameters;
        private readonly PlayerManager _playerManager;
        private readonly DiContainer _diContainer;
        private readonly Transform _parent;
        
        private Vector3 _startPosition = new Vector3(0f, 0f, -5f);

        public PlayerSpawner
        (
            ShipsParameters shipsParameters,
            DiContainer diContainer,
            Transform parent,
            PlayerManager playerManager
        )
        {
            _shipsParameters = shipsParameters;
            _playerManager = playerManager;
            _diContainer = diContainer;
            _parent = parent;
        }

        public void Spawn()
        {
            var shipName = new StringBuilder("Player").Append(_playerManager.CurrentShip + 1).ToString();
            var prefab = Resources.Load<GameObject>("Ships/Player/" + shipName);
            var go = ObjectsPoolManager.Instantiate(prefab, _startPosition);
            _diContainer.InjectGameObject(go);
            go.transform.SetParent(_parent, false);
            go.tag = Tags.PLAYER;
            var baseParameters = _shipsParameters.BaseShipsStats[Tags.PLAYER];
            var multipliers = _shipsParameters.ShipsStatsMultipliers[shipName];
            SetParameters(go, baseParameters, multipliers);
        }
    }
}