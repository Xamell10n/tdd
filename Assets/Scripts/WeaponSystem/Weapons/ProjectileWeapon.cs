﻿using Constants;
using ObjectsPool;
using UnityEngine;
using WeaponSystem.Projectiles;
using Zenject;

namespace WeaponSystem
{
	public class ProjectileWeapon : Weapon
	{
		public GameObject Projectile;

		private Transform _parent;

		[Inject]
		private void Initialize
		(
			Transform parent
		)
		{
			_parent = parent;
		}
		
		public override void Fire()
		{
			var go = ObjectsPoolManager.Instantiate(Projectile, FirePoint.position, _parent);
			go.transform.rotation = transform.rotation;
			go.tag = Tags.PROJECTILE;
			go.GetComponent<Projectile>().SetParameters(_damageMultiplier);
		}
	}
}