﻿using Constants;
using ObjectsPool;
using UnityEngine;

namespace WeaponSystem.Projectiles
{
    [RequireComponent(typeof(Collider))]
    public class ProjectileDestroyer : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tags.PROJECTILE))
            {
                ObjectsPoolManager.Destroy(other.gameObject);
            }
        }
    }}