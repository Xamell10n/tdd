﻿using Managers;
using Tables.PlayerShips;
using UI.Windows;
using UnityEngine;

namespace Installers
{
    public class MainMenuInstaller : BaseMonoInstaller
    {
        [Header("Windows")] 
        [SerializeField] private MainMenuView _mainMenu;
//        [SerializeField] private ShipPickView _shipPick;
        [SerializeField] private ShipPickWithArrowsView _shipPickWithArrows;
        [Header("Ohter")] 
        [SerializeField] private PlayerShips _playerShips;

        protected override void DeclareSignals()
        {
        }

        protected override void BindManagers()
        {
            Container.BindInterfacesAndSelfTo<MenuManager>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<MainMenuManager>().AsSingle();
//            Container.BindInterfacesAndSelfTo<ShipPickManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<ShipPickWithArrowsManager>().AsSingle();
        }

        protected override void BindPrefabs()
        {
            BindWindow(_mainMenu);
            BindWindow(_shipPickWithArrows);
        }

        protected override void BindOther()
        {
            Container.BindInstance(_playerShips).AsSingle();
        }
    }
}