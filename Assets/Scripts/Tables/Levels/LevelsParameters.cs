﻿using System;
using System.Linq;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Tables.Levels
{
	[CreateAssetMenu(menuName = "Scriptable/LevelsParameters", fileName = "LevelsParameters")]
	public class LevelsParameters : ScriptableObject
	{
		[SerializeField] private LevelsParametersDictionary _levelsParams;
		private ReorderableList _levels;

		private LevelParameters[] _parameters;

		private void OnEnable()
		{
			_parameters = _levelsParams.Values.Where(x => x.IsEnable).ToArray();
		}

		public LevelParameters GetParameters(int index)
		{
			return _parameters[index];
		}

		public int Count()
		{
			return _parameters.Length;
		}
	
		[Serializable]
		public class LevelsParametersDictionary : SerializableDictionaryBase<string, LevelParameters>{ }
	}
}
