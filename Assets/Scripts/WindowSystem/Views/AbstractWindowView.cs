﻿using UnityEngine;
using WindowSystem.Interfaces;

namespace WindowSystem.Views
{
    public abstract class AbstractWindowView : MonoBehaviour, IWindowView
    {
        public void Open()
        {
            if (!gameObject.activeInHierarchy)
                gameObject.SetActive(true);
        }

        public void Close()
        {
            if (gameObject.activeInHierarchy)
                gameObject.SetActive(false);
        }

        public abstract void Unlisten();
    }
}