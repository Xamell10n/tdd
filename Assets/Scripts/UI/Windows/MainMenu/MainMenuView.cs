﻿using System;
using UI.Components;
using UnityEngine;
using WindowSystem.Views;

namespace UI.Windows
{
    public class MainMenuView : AbstractWindowView
    {
        [SerializeField] private Button _start;
        [SerializeField] private Button _exit;

        public void Listen(Action onStart, Action onExit)
        {
            _start.Listen(onStart);
            _exit.Listen(onExit);
        }
        
        public override void Unlisten()
        {
            _start.Unlisten();
            _exit.Unlisten();
        }
    }
}