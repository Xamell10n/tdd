﻿namespace WindowSystem.Interfaces
{
    public interface IWindowView
    {
        void Open();
        void Close();
        void Unlisten();
    }
}