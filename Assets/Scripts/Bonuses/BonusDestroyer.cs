﻿using System.Collections;
using System.Collections.Generic;
using Constants;
using ObjectsPool;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class BonusDestroyer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.BONUS))
        {
            ObjectsPoolManager.Destroy(other.gameObject);
        }
    }
}