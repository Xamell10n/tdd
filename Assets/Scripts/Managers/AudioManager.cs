﻿using UnityEngine;

namespace Managers
{
    public class AudioManager
    {
        private readonly AudioSource _audioSource;
        
        public AudioManager
        (
            AudioSource audioSource
        )
        {
            _audioSource = audioSource;
        }
        
        public void PlayBackground(AudioClip audio)
        {
            _audioSource.clip = audio;
            _audioSource.Play();
        }
        
        
    }
}