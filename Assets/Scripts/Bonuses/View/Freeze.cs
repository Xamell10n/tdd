﻿using System.Collections;
using Constants;
using Managers;
using UnityEngine;
using Zenject;

namespace Bonuses
{
    public class Freeze : Bonus
    {
        [SerializeField] private float _time;

        private SignalBus _signalBus;

        [Inject]
        private void Initialize
        (
            SignalBus signalBus
        )
        {
            _signalBus = signalBus;
        }
        
        protected override void Trigger(GameObject target)
        {
            if (target.CompareTag(Tags.PLAYER))
            {
                StartCoroutine(DoFreeze());
            }
        }

        private IEnumerator DoFreeze()
        {
            var freeze = new Signals.EnemyFreeze {IsFreeze = true};
            _signalBus.Fire(freeze);
            yield return new WaitForSeconds(_time);
            freeze.IsFreeze = false;
            _signalBus.Fire(freeze);
            Destroy();
        }
    }
}