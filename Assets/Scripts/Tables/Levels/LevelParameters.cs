﻿using System;
using EnemiesPositionsSetter;
using UnityEngine;

namespace Tables.Levels
{
	[Serializable]
	public class LevelParameters
	{
		public EnemiesPositions EnemiesPositions;
		public string SpriteBackgroundName;
		public string AudioBackgroundName;
		public bool IsEnable = true;
	}
}