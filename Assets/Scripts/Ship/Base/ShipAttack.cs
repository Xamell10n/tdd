﻿using Managers;
using UnityEngine;
using WeaponSystem;

namespace Ship.Base
{
	public abstract class ShipAttack : MonoBehaviour
	{
		public Weapon[] Weapons;
		
		private float _shootSpeed;
		private int _currentWeaponIndex;
		private float _nextTimeForAttack;
		private float _damageMultiplier = 1;

		public void SetShootSpeed(float shootSpeed)
		{
			_shootSpeed = shootSpeed;
		}

		protected void Fire()
		{
			if (Time.time < _nextTimeForAttack) return;
			Weapons[_currentWeaponIndex].SetParameters(_damageMultiplier);
			Weapons[_currentWeaponIndex].Fire();
			_nextTimeForAttack = Time.time + _shootSpeed;
		}

		protected void NextWeapon()
		{
			if (++_currentWeaponIndex >= Weapons.Length)
			{
				_currentWeaponIndex = 0;
			}
			_nextTimeForAttack = Time.time + _shootSpeed;
		}

		public void AddDamage(float percent)
		{
			_damageMultiplier *= 1 + percent / 100;
		}
	}
}