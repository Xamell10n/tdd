﻿using System;
using UnityEngine;
using Utils.TablesDownloader;

namespace Tables._Test
{
    [CreateAssetMenu(menuName = "Scriptable/Test/Items", fileName = "Items")]
    public class Items : DownloadableScriptableObject
    {
        public Item[] ItemsArray;
        
        public override void Clear()
        {
            ItemsArray = new Item[0];
        }
    }

    [Serializable]
    public class Item
    {
        public int Index;
        public string Value;
    }
}
