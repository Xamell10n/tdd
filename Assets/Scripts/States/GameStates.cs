﻿namespace States
{
    public enum GameState
    {
        None,
        Loading,
        Menu,
        Game
    }
}