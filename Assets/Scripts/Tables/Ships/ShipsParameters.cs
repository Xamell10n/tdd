﻿using System;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Tables.Ships
{
    [CreateAssetMenu(menuName = "Scriptable/ShipsParameters", fileName = "ShipsParameters")]
    public class ShipsParameters : ScriptableObject
    {
        public BaseShipsStatsDictionary BaseShipsStats;
        public ShipsStatsMultipliersDictionary ShipsStatsMultipliers;
        
        [Serializable]
        public class BaseShipsStatsDictionary : SerializableDictionaryBase<string, BaseShipsStats> { }

        [Serializable]
        public class ShipsStatsMultipliersDictionary : SerializableDictionaryBase<string, ShipsStatMultipliers> { }
    }
}
