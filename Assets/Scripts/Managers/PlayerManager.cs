﻿namespace Managers
{
    public class PlayerManager
    {
        public int CurrentShip;
        public int Score;
        public int CurrentLevel;

        public void Load()
        {
            Score = 0;
            CurrentShip = 0;
            CurrentLevel = 0;
        }
    }
}