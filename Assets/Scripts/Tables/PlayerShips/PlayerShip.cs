﻿using System;
using UnityEngine;

namespace Tables.PlayerShips
{
    [Serializable]
    public class PlayerShip
    {
        public Sprite Sprite;
    }
}