﻿using System.Collections;
using ObjectsPool;
using UnityEngine;

namespace WeaponSystem.Projectiles
{
    public class SelfAimMissle : Projectile
    {
        public float Damage;
        public float Speed;
        public float TargetLockTime;
        
        protected override void OnHit()
        {
            ObjectsPoolManager.Destroy(gameObject);
        }

        protected override float GetDamage()
        {
            return Damage;
        }

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void OnEnable()
        {
            _rigidbody.velocity = transform.forward * Speed;
            _audioSource.Play();
        }

        private IEnumerator GetTarget()
        {
            yield return new WaitForSeconds(TargetLockTime);
            
        }

        private void SetTarget()
        {
            
        }
    }
}