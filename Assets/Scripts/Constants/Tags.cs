﻿namespace Constants
{
    public class Tags
    {
        public const string ENEMY = "Enemy";
        public const string PLAYER = "Player";
        public const string PROJECTILE = "Projectile";
        public const string BONUS = "Bonus";
    }
}