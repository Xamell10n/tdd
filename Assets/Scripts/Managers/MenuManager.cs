﻿
using UI.Windows;
using WindowSystem.Managers;
using Zenject;

namespace Managers
{
    public class MenuManager : IInitializable
    {
        private readonly PlayerManager _playerManager;
        private readonly WindowsManager _windowsManager;
        
        public MenuManager
        (
            PlayerManager playerManager,
            WindowsManager windowsManager
        )
        {
            _playerManager = playerManager;
            _windowsManager = windowsManager;
        }
        
        public void Initialize()
        {
            _windowsManager.OpenWindow<MainMenuManager>();
            Load();
        }
        
        private void Load()
        {
            _playerManager.Load();
        }
    }
}