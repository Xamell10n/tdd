﻿using UnityEngine;
using WindowSystem.Views;
using Zenject;

public abstract class BaseMonoInstaller : MonoInstaller<BaseMonoInstaller>
{
    private Transform _rootTransform;
    
    public override void InstallBindings()
    {
        FindRootCanvas();
        
        DeclareSignals();
        BindManagers();
        BindPrefabs();
        BindOther();
    }

    protected abstract void DeclareSignals();
    protected abstract void BindManagers();
    protected abstract void BindPrefabs();
    protected abstract void BindOther();

    private void FindRootCanvas()
    {
        _rootTransform = GameObject.FindWithTag("RootCanvas").transform;
    }
    
    protected void BindWindow<T>(T prefab) where T : AbstractWindowView
    {
        var go = Instantiate(prefab, _rootTransform, false);
        go.gameObject.SetActive(false);
        Container.BindInstance(go).AsSingle();
    }

}