﻿namespace WindowSystem.Interfaces
{
    public interface IWindowManager
    {
        void Open();
        void Close();
        void Listen();
        void Unlisten();
    }
}