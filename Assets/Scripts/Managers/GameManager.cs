﻿using Bonuses;
using CustomGUI.Windows;
using UI.Windows.LevelComplete;
using WindowSystem.Managers;
using Zenject;

namespace Managers
{
    public class GameManager : IInitializable, ILateDisposable
    {
        private readonly LevelManager _levelManager;
        private readonly PlayerManager _playerManager;
        private readonly HUDManager _hud;
        private readonly SignalBus _signalBus;
        private readonly WindowsManager _windowsManager;
        private readonly BonusesManager _bonusesManager;

        private int _score;
        private int _shipsDestroyed;

        public GameManager
        (
            LevelManager levelManager,
            HUDManager hud,
            SignalBus signalBus,
            WindowsManager windowsManager,
            BonusesManager bonusesManager,
            PlayerManager playerManager
        )
        {
            _levelManager = levelManager;
            _playerManager = playerManager;
            _hud = hud;
            _signalBus = signalBus;
            _windowsManager = windowsManager;
            _bonusesManager = bonusesManager;
        }

        public void Initialize()
        {
            Listen();
            _bonusesManager.Initialize();
            _levelManager.Initialize();
            _score = _playerManager.Score;
            _windowsManager.OpenWindow<HUDManager>();
            UpdateScore();
            UpdateGoals();
        }

        public void LateDispose()
        {
            Unlisten();
        }

        private void Listen()
        {
            _signalBus.Subscribe<Signals.PlayerDestroy>(DestroyPlayer);
            _signalBus.Subscribe<Signals.EnemyDestroy>(DestroyEnemy);
        }

        private void Unlisten()
        {
            _signalBus.Unsubscribe<Signals.PlayerDestroy>(DestroyPlayer);
            _signalBus.Unsubscribe<Signals.EnemyDestroy>(DestroyEnemy);
        }

        private void DestroyEnemy(Signals.EnemyDestroy enemy)
        {
            _score += enemy.Score;
            UpdateScore();
            _shipsDestroyed++;
            UpdateGoals();
            if (_levelManager.MaxEnemies <= _shipsDestroyed)
            {
                Win();
            }
        }

        private void Win()
        {
            _playerManager.Score = _score;
            bool isLastLevel;
            _levelManager.LevelComplete(out isLastLevel);
            if (isLastLevel)
            {
                _windowsManager.OpenWindow<LevelCompleteManager, LevelCompleteManager.States>(
                    LevelCompleteManager.States.CompleteLast);
            }
            else
            {
                _windowsManager.OpenWindow<LevelCompleteManager, LevelCompleteManager.States>(
                    LevelCompleteManager.States.Complete);
            }
        }

        private void DestroyPlayer(Signals.PlayerDestroy player)
        {
            _playerManager.Score = _score;
            _windowsManager.OpenWindow<LevelCompleteManager, LevelCompleteManager.States>(
                LevelCompleteManager.States.Fail);
        }

        private void UpdateScore()
        {
            _hud.UpdateScore(_score);
        }

        private void UpdateGoals()
        {
            _hud.UpdateGoal(_shipsDestroyed, _levelManager.MaxEnemies);
        }
    }
}