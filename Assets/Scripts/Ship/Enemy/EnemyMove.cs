﻿using System.Collections;
using Ship.Base;
using UnityEngine;

namespace Ship.Enemy
{
    public class EnemyMove : ShipMove
    {
        public void MoveLeft()
        {
            MoveToPosition(transform.position - 100f * transform.right);
        }

        public void MoveRight()
        {
            MoveToPosition(transform.position + 100f * transform.right);
        }

        private void MoveToPosition(Vector3 position)
        {
            Stop();
            StartCoroutine(DoMoveToPosition(position));
        }

        public void Stop()
        {
            StopAllCoroutines();
        }

        private IEnumerator DoMoveToPosition(Vector3 position)
        {
            var delta = position - transform.position;
            while (delta.sqrMagnitude > 0.1f)
            {
                _rigidbody.MovePosition(transform.position +
                                        delta.normalized * _currentSpeed * Time.fixedDeltaTime);
//                delta = position - transform.position;
                yield return new WaitForFixedUpdate();
            }
            _rigidbody.MovePosition(position);
        }
    }
}