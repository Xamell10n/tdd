﻿namespace UI.Windows.LevelComplete
{
    public class Constants
    {
        public const string COMPLETE = "Level complete!";
        public const string COMPLETE_LAST = "Congratulations! You won the game!";
        public const string FAIL = "You lose! Try next time";
    }
}