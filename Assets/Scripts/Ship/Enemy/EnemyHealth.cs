﻿using Managers;
using Ship.Base;
using Zenject;

namespace Ship.Enemy
{
    public class EnemyHealth : ShipHealth
    {
        private SignalBus _signalBus;

        [Inject]
        private void Initialize
        (
            SignalBus signalBus
        )
        {
            _signalBus = signalBus;
        }

        protected override void OnDie()
        {
            _signalBus.Fire(new Signals.EnemyDestroy
            {
                Score = (int) Score,
                Position = transform.position
            });
        }
    }
}