﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI.Windows
{
    public class ShipButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Image _image;
        
        private int _index = -1;
        private Action<int> _callback;
        
        public void Listen(int index, Action<int> callback)
        {
            _index = index;
            _callback = callback;
        }

        public void SetShip(Sprite sprite)
        {
            _image.sprite = sprite;
        }

        public void Unlisten()
        {
            _index = -1;
            _callback = null;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_callback != null)
                _callback(_index);
        }

    }
}