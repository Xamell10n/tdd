﻿using Ship.Base;
using UnityEngine;

namespace Ship.Player
{
    public class PlayerMove : ShipMove
    {
        private void FixedUpdate()
        {
            var x = Input.GetAxisRaw("Horizontal");
            if (Mathf.Abs(x) < 0.01) return;
            var delta = new Vector3(x * _currentSpeed * Time.fixedDeltaTime, 0, 0);
            DeltaMove(delta);
        }
    }
}