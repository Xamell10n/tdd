﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;
using WindowSystem.Views;

namespace CustomGUI.Windows
{
    public class HUDView : AbstractWindowView
    {
        [SerializeField] private Text _score;
        [SerializeField] private Text _goal;

        public override void Unlisten()
        {
            
        }

        public void SetScore(int score)
        {
            _score.text = score.ToString();
        }

        public void SetGoal(int current, int goal)
        {
            _goal.text = new StringBuilder(current.ToString()).Append("/").Append(goal).ToString();
        }
    }
}