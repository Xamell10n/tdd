﻿using System;
using System.Collections;
using UnityEditor;

namespace Utils.TablesDownloader
{
    public class SpreadsheetDownloader : UnityEditor.Editor
    {
        private IEnumerator _enumerator;

        protected virtual string Sheet { get; private set; }
        public bool DownLoading;
        protected DownloadableScriptableObject CurrentScriptableObject;

        protected void Load(string sheet)
        {
            StartCoroutine(Connect(sheet));
        }

        protected virtual void OnInitialDownLoad(string result)
        {
        }

        public void InitialLoad(DownloadableScriptableObject scriptableObject)
        {
            CurrentScriptableObject = scriptableObject;
            Load(Sheet);
        }

        private IEnumerator Connect(string sheet)
        {
            var settingsObject = AssetDatabase.FindAssets("SpreadsheetsSettings");
            if (settingsObject.Length == 0)
                throw new Exception(
                    "Create SpreadsheetsSettings with name \"SpreadsheetsSettings\"");
            var settings =
                AssetDatabase.LoadAssetAtPath<SpreadsheetSettings>(
                    AssetDatabase.GUIDToAssetPath(settingsObject[0]));
            var connector = new SpreadsheetConnection(settings);
            var spreadsheet = new Spreadsheet(connector);
            return spreadsheet.GetTableMapped(sheet, OnDownload);
        }

        private void StartCoroutine(IEnumerator enumerator)
        {
            _enumerator = enumerator;
            EditorApplication.update += Coroutine;
        }

        private void Coroutine()
        {
            if (_enumerator.MoveNext())
            {
                if (_enumerator.Current != null)
                    _enumerator = (IEnumerator) _enumerator.Current;
            }
            else
            {
                EditorApplication.update -= Coroutine;
            }
        }

        private void OnDownload(string result)
        {
            OnInitialDownLoad(result);
        }

        protected void Save()
        {
//			serializedObject.ApplyModifiedProperties();
            if (CurrentScriptableObject == null)
                EditorUtility.SetDirty(target);
            else
                EditorUtility.SetDirty(CurrentScriptableObject);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}