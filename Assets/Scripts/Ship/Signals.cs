﻿using UnityEngine;

namespace Managers
{
    public class Signals
    {
        public class EnemyDestroy
        {
            public int Score;
            public Vector3 Position;
        }
        
        public class PlayerDestroy
        {
            
        }
        
        public class EnemyTurn
        {
            public bool IsMoveRight;
        }
        
        public class EnemyFreeze
        {
            public bool IsFreeze;
        }
        
        public class Pause
        {
            public bool IsPause;
        }

    }
}