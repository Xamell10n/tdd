﻿using System;
using UnityEngine;

namespace Tables.Ships
{
	[Serializable]
	public class ShipsStatMultipliers
	{
		[Range(0, 10)] public float Health;
		[Range(0, 10)] public float MoveSpeed;
		[Range(0, 10)] public float ShootSpeed;
		[Range(0, 10)] public float Mobility;
		[Range(0, 10)] public float WeaponDamage;
		[Range(0, 10)] public float Score;
	}
}