﻿using UnityEngine;

namespace Utils.TablesDownloader {
	[CreateAssetMenu(menuName = "Scriptable/Other/Spreadsheet settings", fileName = "SpreadsheetsSettings")]
	public class SpreadsheetSettings : ScriptableObject {
		public string Url =
			"https://script.google.com/macros/s/AKfycbx0vKokUNp01fqLhFQ9vFwzchIerDn_q-6ONmTOHVb5F7dUK7DJ/exec";
		public string Spreadsheet = "1QsdqxoInNpSYfOB2A5BTlYnqADX4Cdn1LwaYc4p0URA";
		public string Password = "passcode";
		public float Timeout = 20f;
	}
}