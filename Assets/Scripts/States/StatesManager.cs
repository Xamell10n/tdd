﻿using Managers;

namespace States
{
    public class StatesManager
    {
        public static GameState CurrentState { get; private set; }

        public static void ChangeState(GameState state)
        {
            if (CurrentState == state) return;
            OnChangeEnd(CurrentState);
            OnChangeStart(state);
            CurrentState = state;
        }

        private static void OnChangeEnd(GameState state)
        {
            
        }

        private static void OnChangeStart(GameState state)
        {
            switch (state)
            {
                    case GameState.Loading:
                        break;
                    case GameState.Menu:
                        PauseManager.SetPause(true);
                        break;
                    case GameState.Game:
                        PauseManager.SetPause(false);
                        break;
            }
        }
    }
}