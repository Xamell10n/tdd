﻿using Managers;
using Tables.Bonuses;
using UnityEngine;
using Zenject;

namespace Bonuses
{
    public class BonusesManager : ILateDisposable
    {
        private readonly SignalBus _signalBus;
        private readonly BonusesParameters _bonusesParameters;
        private readonly BonusSpawner _bonusSpawner;
        
        public BonusesManager
        (
            SignalBus signalBus,
            BonusSpawner bonusSpawner,
            BonusesParameters bonusesParameters
        )
        {
            _signalBus = signalBus;
            _bonusesParameters = bonusesParameters;
            _bonusSpawner = bonusSpawner;
        }

        public void Initialize()
        {
            Listen();
        }

        public void LateDispose()
        {
            Unlisten();
        }

        private void OnEnemyDestroy(Signals.EnemyDestroy enemy)
        {
            SpawnBonus(enemy.Position);
        }

        private void SpawnBonus(Vector3 position)
        {
            var name = _bonusesParameters.GetRandomBonus();
            if (name == Constants.NONE)
            {
                return;
            }
            _bonusSpawner.Spawn(name, position, _bonusesParameters.Speed);
        }

        private void Listen()
        {
            _signalBus.Subscribe<Signals.EnemyDestroy>(OnEnemyDestroy);
        }

        private void Unlisten()
        {
            _signalBus.Unsubscribe<Signals.EnemyDestroy>(OnEnemyDestroy);
        }
        
    }
}