﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.TablesDownloader
{
    public class Spreadsheet
    {
        private readonly SpreadsheetConnection _connection;

        private const string MsgObjData = "OBJ_DATA";
        private const string MsgTblData = "TBL_DATA";
        private const string MsgTblsData = "TBLS_DATA";

        private const string TypeEnd = "_ENDTYPE\n";
        private const string TypeStart = "TYPE_";

        public Spreadsheet(SpreadsheetConnection connection)
        {
            _connection = connection;
        }


        private void Log(string log)
        {
            Debug.Log(log);
        }

        /// <summary>
        /// Will create a new table (new worksheet on the spreadsheet) with the specified name and headers.
        /// Expects an array of field names/headers, as well as the type/table name to be used.
        /// </summary>
        /// <param name="fields">String array with the names of the table headers.</param>
        /// <param name="tableTypeName">The name of the table to be created.</param>
        public IEnumerator AddSheet(string[] headers, string tableTypeName)
        {
            Dictionary<string, string> form = new Dictionary<string, string>();
            form.Add("action", "createObject");
            form.Add("type", tableTypeName);
            form.Add("num", headers.Length.ToString());

            for (int i = 0; i < headers.Length; i++)
            {
                form.Add("field" + i, headers[i]);
            }

            yield return _connection.Create(form, Log);
        }

        /// <summary>
        /// Retrieves from the spreadsheet an array of all the objects found in the specified table.
        /// Expects the table name.
        /// </summary>
        /// <param name="name">The name of the table to be retrieved.</param>
        public IEnumerator GetTable(string name, Action<string> onComplete)
        {
            yield return _connection.Create(
                new Dictionary<string, string> {{"action", "getTable"}, {"type", name}},
                x => UnpackTable(x, onComplete));
        }

        public IEnumerator GetTableMapped(string name, Action<string> onComplete)
        {
            yield return _connection.Create(
                new Dictionary<string, string> {{"action", "GetTableMapped"}, {"type", name}},
                x => UnpackTable(x, onComplete));
        }

        /// <summary>
        /// Retrieves from the spreadsheet the data from all tables, in the form of one or more array of objects.
        /// <summary>
        public IEnumerator GetAllSheets()
        {
            Dictionary<string, string> form = new Dictionary<string, string>();
            form.Add("action", "getAllTables");
            ;
            yield return _connection.Create(form, UnpackTables);
        }

        /// <summary>
        /// Will create a new object (new row in a worksheet) with the specified field values.
        /// Expects a dictionary of field names & values to be stored, as well as the type/table name.
        /// If not all type fields available in the table are passed, those missing will be filled as "null".
        /// Fields passed but not in the table, will be ignored.
        /// </summary>
        /// <param name="fields">Dictionary of field names & values to be stored.</param>
        /// <param name="typeName">Name of the table that will hold the object.</param>
        public IEnumerator AddRow(string typeName, Dictionary<string, string> fields)
        {
            var form = fields;
            form.Add("type", typeName);
            form.Add("action", "createObject");
            form.Add("isJson", "false");
            yield return _connection.Create(form, Log);
        }

        /// <summary>
        /// Retrieves from the spreadsheet an array of objects found by searching with the specified criteria.
        /// Expects the table name, the name of the field to search by, and the value to search.
        /// </summary>
        /// <param name="objTypeName">Name of the table to search.</param>
        /// <param name="searchFieldName">Name of the field to search by.</param>
        /// <param name="searchValue">Value to search for.</param>
        public IEnumerator GetRowByField(
            string objTypeName,
            string searchFieldName,
            string searchValue)
        {
            Dictionary<string, string> form = new Dictionary<string, string>();
            form.Add("action", "GetObjects");
            form.Add("type", objTypeName);
            form.Add(searchFieldName, searchValue);
            form.Add("search", searchFieldName);

            yield return _connection.Create(form, UnpackObject);
        }


        /// <summary>
        /// Updates a field in one or more objects found by searching with the specified criteria.
        /// Expects the table name, the name of the field to search by, and the value to search,
        /// as well as the field name and value to be updated in the matching objects.
        /// </summary>
        /// <param name="name">Name of the table to search.</param>
        /// <param name="searchFieldName">Name of the field to search by.</param>
        /// <param name="searchValue">Value to search for.</param>
        /// <param name="fieldNameToUpdate">Name of the field to update.</param>
        /// <param name="updateValue">New value to be set.</param>
        public IEnumerator UpdateRows(
            string name,
            string searchFieldName,
            string searchValue,
            string fieldNameToUpdate,
            string updateValue)
        {
            Dictionary<string, string> form = new Dictionary<string, string>();
            form.Add("action", "updateObjects");
            form.Add("type", name);
            form.Add("searchField", searchFieldName);
            form.Add("searchValue", searchValue);
            form.Add("updtField", fieldNameToUpdate);
            form.Add("updtValue", updateValue);

            yield return _connection.Create(form, Log);
        }

        /// <summary>
        /// Deletes one or more objects (rows) from a table (worksheet) found by searching with the specified criteria.
        /// Expects the table name, the name of the field to search by, and the value to search.
        /// </summary>
        /// <param name="objTypeName">Name of the table to search.</param>
        /// <param name="searchFieldName">Name of the field to search by.</param>
        /// <param name="searchValue">Value to search for.</param>
        public IEnumerator DeleteRows(string objTypeName,
            string searchFieldName,
            string searchValue)
        {
            Dictionary<string, string> form = new Dictionary<string, string>();
            form.Add("action", "deleteObjects");
            form.Add("type", objTypeName);
            form.Add(searchFieldName, searchValue);
            form.Add("search", searchFieldName);

            yield return _connection.Create(form, Log);
        }

        private void UnpackTables(string response)
        {
            var objTypeName = new List<string>();
            var jsonData = new List<string>();
            var parsed = response.Substring(MsgTblsData.Length + 1);
            // First split creates substrings containing type and content on each one.
            string[] separator = {TypeStart};
            var split = parsed.Split(separator, System.StringSplitOptions.None);

            // Second split gives the final lists of type names and data on different lists.
            separator = new[] {TypeEnd};
            for (int i = 0; i < split.Length; i++)
            {
                if (split[i] == "")
                    continue;

                string[] secSplit = split[i].Split(separator, System.StringSplitOptions.None);
                objTypeName.Add(secSplit[0]);
                jsonData.Add(secSplit[1]);
            }
            for (var index = 0; index < jsonData.Count; index++)
            {
//				D.Log(objTypeName[index], jsonData[index]);
            }
//			D.Log("UnpackTables -> Done", response);
            Debug.Log("UnpackTables -> Done");
        }

        private void UnpackTable(string response, Action<string> onComplete = null)
        {
            var names = new List<string>();
            var json = new List<string>();
            var parsed = response.Substring(MsgTblData.Length + 1);
            names.Add(parsed.Substring(0, parsed.IndexOf(TypeEnd)));
            json.Add(parsed.Substring(parsed.IndexOf(TypeEnd) + TypeEnd.Length));
//				D.Log(names[0], json[0]);
//				D.Log("UnpackTable -> Done");
            Debug.Log("UnpackTable -> Done");
            if (onComplete != null)
                onComplete(json[0]);
        }

        private void UnpackObject(string response)
        {
            var names = new List<string>();
            var json = new List<string>();
            // Response for GetRowByField()
            var parsed = response.Substring(MsgObjData.Length + 1);
            names.Add(parsed.Substring(0, parsed.IndexOf(TypeEnd)));
            json.Add(parsed.Substring(parsed.IndexOf(TypeEnd) + TypeEnd.Length));
//            D.Log(names[0], json[0]);
//            D.Log("UnpackObject -> Done");
            Debug.Log("UnpackTable -> Done");
        }
    }
}