﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Utils.TablesDownloader
{
    public class EditorDownloadAll : EditorWindow
    {
        private TablesSettings _settings;
        private bool _isLoad;

        [MenuItem("Tables/Download Manager")]
        static void Download()
        {
            GetWindow(typeof(EditorDownloadAll));
        }

        private void Awake()
        {
            if (_settings == null)
            {
                var settingsObject = AssetDatabase.FindAssets("TablesDownloadSettings");
                if (settingsObject.Length == 0)
                    throw new Exception(
                        "Create TablesSettings with name \"TablesDownloadSettings\"");
                if (settingsObject.Length > 1)
                    throw new Exception(
                        "Too many files with name \"TablesDownloadSettings\"");
                _settings =
                    AssetDatabase.LoadAssetAtPath<TablesSettings>(
                        AssetDatabase.GUIDToAssetPath(settingsObject[0]));
//                _settings = Resources.Load<TablesSettings>("TablesSetting");
                _isLoad = true;
            }
        }

        private void OnGUI()
        {
            if (!_isLoad) return;
            var serializedObject = new SerializedObject(_settings);
            GUILayout.Label("--- IsParallel ---");
            _settings.IsParallel =
                EditorGUILayout.Toggle(_settings.IsParallel);

            GUILayout.Label("--- Tables ---");
            for (var i = 0; i < _settings.Settings.Count; i++)
            {
                GUILayout.BeginHorizontal();
                _settings.Settings[i].IsDownload =
                    EditorGUILayout.Toggle(_settings.Settings[i].IsDownload);
                _settings.Settings[i].Table =
                    (DownloadableScriptableObject) EditorGUILayout.ObjectField(
                        _settings.Settings[i].Table, typeof(DownloadableScriptableObject), false);
                GUILayout.EndHorizontal();
            }
            GUILayout.Label("--- Buttons ---");
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add"))
            {
                Add();
            }
            if (GUILayout.Button("Delete"))
            {
                Delete();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Download Selected"))
            {
                DownLoadSelected();
            }
            if (GUILayout.Button("Download All"))
            {
                DownloadAll();
            }
            GUILayout.EndHorizontal();
            serializedObject.ApplyModifiedProperties();
            EditorUtility.SetDirty(_settings);
        }

        private void Add()
        {
            _settings.Settings.Add(new TablesSettings.TableSettings());
        }

        private void Delete()
        {
            _settings.Settings.RemoveAt(_settings.Settings.Count - 1);
        }

        private void Clear()
        {
            _settings.Settings.Clear();
        }

        private void DownLoadSelected()
        {
            var list = _settings.Settings.Where(x => x.IsDownload).ToList();
            Download(list);
        }

        private void DownloadAll()
        {
            Download(_settings.Settings);
        }

        private void Download(List<TablesSettings.TableSettings> list)
        {
            if (_settings.IsParallel)
            {
                DownloadParallel(list);
            }
            else
            {
                DownloadConsistently(list);
            }

        }

        private void DownloadParallel(List<TablesSettings.TableSettings> list)
        {
            foreach (var model in list)
            {
                DownloadTable(model);
            }
        }

        private void DownloadConsistently(List<TablesSettings.TableSettings> list)
        {
            foreach (var model in list)
            {
                DownloadTable(model);
                Debug.Log("Done");
            }
        }

        private void DownloadTable(TablesSettings.TableSettings settings)
        {
//            settings.Table.Clear();
            var str = settings.Table.SpreadSheetDownloader;
            var type = Type.GetType(str);
            if (type == null)
            {
                Debug.LogError("Error in DownloadTable");
                return;
            }
            var obj = (SpreadsheetDownloader) Activator.CreateInstance(type);
            obj.InitialLoad(settings.Table);
        }
    }
}