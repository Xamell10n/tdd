﻿using System;
using System.Collections.Generic;
using WindowSystem.Interfaces;

namespace WindowSystem.Managers
{
    public class WindowsManager
    {
        private readonly Stack<IWindowManager> _stack = new Stack<IWindowManager>();
        private readonly WindowsFabric _windowsFabric;

        private Action _onOpenWindow;
        
        public WindowsManager
        (
            WindowsFabric windowsFabric
        )
        {
            _windowsFabric = windowsFabric;
        }
        
        public void OpenWindow<T>() where T : IWindowManager
        {
            if (_stack.Count > 0)
            {
                _stack.Peek().Unlisten();
                if (_stack.Peek() is IPopup)
                {
                    
                }
                else
                {
                    _stack.Peek().Close();
                }
            }
            var window = _windowsFabric.Window<T>();
            Open(window);
        }

        public void OpenWindow<T, TX>(TX data) where T : IWindowManager, IOpen<TX>
        {
            _onOpenWindow = () => ((IOpen<TX>) _stack.Peek()).Open(data);
            OpenWindow<T>();
        }

        public void CloseWindow()
        {
            Close();
        }

        private void Open(IWindowManager window)
        {
            _stack.Push(window);
            _stack.Peek().Open();
            if (_onOpenWindow != null)
            {
                _onOpenWindow();
                _onOpenWindow = null;
            }
            _stack.Peek().Listen();
        }

        private void Close()
        {
            _stack.Peek().Unlisten();
            _stack.Pop().Close();
            _stack.Peek().Open();
            _stack.Peek().Listen();
        }
    }
}