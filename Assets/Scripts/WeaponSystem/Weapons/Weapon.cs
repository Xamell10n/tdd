﻿using UnityEngine;
using WeaponSystem.Projectiles;

namespace WeaponSystem
{
	public abstract class Weapon : MonoBehaviour
	{
		public Transform FirePoint;
		protected float _damageMultiplier;

		public void SetParameters(float damageMultiplier)
		{
			_damageMultiplier = damageMultiplier;
		}
		
		public abstract void Fire();
	}
}