﻿using Ship.Base;
using UnityEngine;

namespace WeaponSystem.Projectiles
{
    [RequireComponent(typeof(Rigidbody))]
    public abstract class Projectile : MonoBehaviour
    {
        protected AudioSource _audioSource;
        protected Rigidbody _rigidbody;
        protected float _damageMultiplier;
        
        protected abstract void OnHit();
        protected abstract float GetDamage();

        public void SetParameters(float damageMultiplier)
        {
            _damageMultiplier = damageMultiplier;
        }

        private void OnTriggerEnter(Collider other)
        {
            var health = other.GetComponent<ShipHealth>();
            if (health)
            {
                if (health.Hit(GetDamage()))
                {
                    OnHit();
                }
            }
        }
    }
}