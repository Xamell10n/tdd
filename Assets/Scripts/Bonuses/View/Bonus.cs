﻿using System.Collections;
using Constants;
using ObjectsPool;
using UnityEngine;

namespace Bonuses
{
    [RequireComponent(typeof(Rigidbody)), RequireComponent(typeof(Collider))]
    public abstract class Bonus : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;
        [SerializeField] private AudioClip _spawn;
        [SerializeField] private AudioClip _pick;

        private AudioSource _audioSource;
        private Rigidbody _rigidbody;
        private Collider _collider;
        private Vector3 _delta;

        protected abstract void Trigger(GameObject target);

        public void SetSpeed(float speed)
        {
            _delta = Vector3.back * speed * Time.fixedDeltaTime;
        }
        
        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();
            _audioSource = GetComponent<AudioSource>();
        }

        private void FixedUpdate()
        {
            _rigidbody.MovePosition(transform.position + _delta);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tags.PLAYER))
            {
                _renderer.enabled = false;
                _collider.enabled = false;
                Trigger(other.gameObject);
            } 
        }

        protected void Destroy()
        {
            StartCoroutine(DoDestroy());
        }

        private IEnumerator DoDestroy()
        {
            _audioSource.clip = _pick;
            _audioSource.Play();
            var time = _pick.length;
            yield return new WaitForSeconds(time + 0.5f);
            ObjectsPoolManager.Destroy(gameObject);
        }

        private void OnEnable()
        {
            _audioSource.clip = _spawn;
            _audioSource.Play();
        }
    }
}