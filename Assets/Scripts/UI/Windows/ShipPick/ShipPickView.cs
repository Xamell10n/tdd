﻿using System;
using UnityEngine;
using UnityEngine.UI;
using WindowSystem.Views;
using Button = UI.Components.Button;

namespace UI.Windows
{
    public class ShipPickView : AbstractWindowView
    {
        [SerializeField] private ShipButton _shipButtonPrefab;
        [SerializeField] private Transform _buttonsParent;
        [SerializeField] private Button _start;
        [SerializeField] private Button _back;
        
        private ShipButton[] _ships;
        
        public override void Unlisten()
        {
            foreach (var ship in _ships)
            {
                ship.Unlisten();
            }
            _start.Unlisten();
            _back.Unlisten();
        }

        public void Listen(Action<int> onShip, Action onStart, Action onBack)
        {
            for (var i = 0; i < _ships.Length; i++)
            {
                _ships[i].Listen(i, onShip);
            }
            _start.Listen(onStart);
            _back.Listen(onBack);
        }

        public void SetShips(Sprite[] sprites)
        {
            if (_ships == null || _ships.Length != sprites.Length)
            {
                var layoutGroup = _buttonsParent.GetComponent<GridLayoutGroup>();
                layoutGroup.enabled = true;
                _ships = new ShipButton[sprites.Length];
                for (var i = 0; i < sprites.Length; i++)
                {
                    _ships[i] = Instantiate(_shipButtonPrefab, _buttonsParent);
                }
//                layoutGroup.enabled = false;
            }
            
            for (var i = 0; i < _ships.Length; i++)
            {
                _ships[i].SetShip(sprites[i]);
            }
        }
    }
}