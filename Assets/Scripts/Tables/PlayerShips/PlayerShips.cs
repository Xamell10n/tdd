﻿using System;
using System.Linq;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Tables.PlayerShips
{
    [CreateAssetMenu(menuName = "Scriptable/PlayerShips", fileName = "Playerships")]
    public class PlayerShips : ScriptableObject
    {
        [SerializeField] private PlayerShipsDictionary _ships;

        private Sprite[] _sprites;

        public Sprite[] GetSprites()
        {
            return _sprites;
        }

        public Sprite GetSprite(int index)
        {
            return _sprites[index];
        }

        public int GetCount()
        {
            return _sprites.Length;
        }

        private void OnEnable()
        {
            _sprites = _ships.Values.Select(x => x.Sprite).ToArray();
        }
    }
    
    [Serializable]
    public class PlayerShipsDictionary : SerializableDictionaryBase<string, PlayerShip> { }
}
