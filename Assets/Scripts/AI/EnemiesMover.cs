﻿using System.Collections.Generic;
using Managers;
using Ship.Enemy;
using Zenject;

namespace AI
{
    public class EnemiesMover : ILateDisposable
    {
        private readonly SignalBus _signalBus;
        
        private List<EnemyMove> _enemies = new List<EnemyMove>();
        private bool _isMoveRight;

        public EnemiesMover
        (
            SignalBus signalBus
        )
        {
            _signalBus = signalBus;
        }
        
        public void LateDispose()
        {
            Unlisten();
        }

        private void Listen()
        {
            _signalBus.Subscribe<Signals.EnemyTurn>(OnEnemyTurn);
            _signalBus.Subscribe<Signals.EnemyFreeze>(OnFreeze);
        }

        private void Unlisten()
        {
            _signalBus.Unsubscribe<Signals.EnemyTurn>(OnEnemyTurn);
            _signalBus.Unsubscribe<Signals.EnemyFreeze>(OnFreeze);
        }

        public void Add(EnemyMove enemy)
        {
            _enemies.Add(enemy);
        }

        public void Start()
        {
            Listen();
            MoveRight();
        }

        private void MoveLeft()
        {
            _isMoveRight = false;
            foreach (var enemy in _enemies)
            {
                if (enemy.gameObject.activeInHierarchy)
                    enemy.MoveLeft();
            }
        }

        private void MoveRight()
        {
            _isMoveRight = true;
            foreach (var enemy in _enemies)
            {
                if (enemy.gameObject.activeInHierarchy)
                    enemy.MoveRight();
            }
        }

        private void Stop()
        {
            foreach (var enemy in _enemies)
            {
                if (enemy.gameObject.activeInHierarchy)
                    enemy.Stop();
            }
        }

        private void OnEnemyTurn(Signals.EnemyTurn enemy)
        {
            OnEnemyTurn(enemy.IsMoveRight);
        }

        private void OnEnemyTurn(bool isMoveRight)
        {
            if (isMoveRight)
            {
                MoveRight();
            }
            else
            {
                MoveLeft();
            }
        }

        private void OnFreeze(Signals.EnemyFreeze enemy)
        {
            if (enemy.IsFreeze)
            {
                Stop();
            }
            else
            {
                OnEnemyTurn(_isMoveRight);
            }
        }
    }
}