﻿using Managers;
using Tables.PlayerShips;
using WindowSystem.Managers;

namespace UI.Windows
{
    public class ShipPickManager : AbstractWindowManager<ShipPickView>
    {
        private readonly WindowsManager _windowsManager;
        private readonly PlayerShips _playerShips;
        private readonly PlayerManager _playerManager;

        private int _currentShip;

        public ShipPickManager
        (
            ShipPickView view,
            WindowsManager windowsManager,
            PlayerManager playerManager,
            PlayerShips playerShips
        ) : base(view)
        {
            _windowsManager = windowsManager;
            _playerShips = playerShips;
            _playerManager = playerManager;
        }
        
        public override void Listen()
        {
            View.SetShips(_playerShips.GetSprites());
            View.Listen(OnShip, OnStart, OnBack);
        }

        private void OnShip(int index)
        {
            _currentShip = index;
        }

        private void OnStart()
        {
            _playerManager.CurrentShip = _currentShip;
            SceneManager.Load(1);
        }

        private void OnBack()
        {
            _windowsManager.CloseWindow();
        }
    }
}