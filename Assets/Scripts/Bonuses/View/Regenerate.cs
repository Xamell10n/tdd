﻿using System.Collections;
using Ship.Base;
using UnityEngine;

namespace Bonuses
{
    public class Regenerate : Bonus
    {
        [SerializeField] private float _time;
        [SerializeField] private float _period;
        [SerializeField] private float _value;

        protected override void Trigger(GameObject target)
        {
            var health = target.GetComponent<ShipHealth>();
            if (health)
            {
                StartCoroutine(DoRegenerate(health));
            }
        }

        private IEnumerator DoRegenerate(ShipHealth health)
        {
            var count = _time / _period;
            var delta = _value / count;
            for (var i = 0; i < count; i++)
            {
                yield return new WaitForSeconds(_period);
                health.Heal(delta);
            }
            Destroy();
        }
    }
}