﻿using System;
using CustomGUI;
using UI.Components;
using UnityEngine;
using UnityEngine.UI;
using WindowSystem.Views;
using Button = UI.Components.Button;

namespace UI.Windows.LevelComplete
{
    public class LevelCompleteView : AbstractWindowView
    {
        [SerializeField] private Text _text;
        [SerializeField] private Text _score;
        [SerializeField] private Button _nextLevel;
        [SerializeField] private Button _exit;
        [SerializeField] private Button _restart;

        public void Listen(Action onNextLevel, Action onExit, Action onRestart)
        {
            _nextLevel.Listen(onNextLevel);
            _exit.Listen(onExit);
            _restart.Listen(onRestart);
        }
        
        public override void Unlisten()
        {
            _nextLevel.Unlisten();
            _exit.Unlisten();
            _restart.Unlisten();
        }

        public void SetLevelComplete()
        {
            _nextLevel.gameObject.SetActive(true);
            _exit.gameObject.SetActive(true);
            _restart.gameObject.SetActive(false);
        }
        
        public void SetLevelCompleteLast()
        {
            _nextLevel.gameObject.SetActive(false);
            _exit.gameObject.SetActive(true);
            _restart.gameObject.SetActive(false);
        }
        
        public void SetLevelFail()
        {
            _nextLevel.gameObject.SetActive(false);
            _exit.gameObject.SetActive(true);
            _restart.gameObject.SetActive(true);
        }

        public void SetText(string text)
        {
            _text.text = text;
        }
        
        public void SetScore(int score)
        {
            _score.text = score.ToString();
        }
    }
}