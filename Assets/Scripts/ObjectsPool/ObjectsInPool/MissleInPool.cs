﻿using UnityEngine;

namespace ObjectsPool
{
    public class MissleInPool : ObjectInPool
    {
        public override void Reset()
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}