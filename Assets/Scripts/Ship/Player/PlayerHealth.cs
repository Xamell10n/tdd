﻿using Managers;
using Ship.Base;
using Zenject;

namespace Ship.Player
{
    public class PlayerHealth : ShipHealth
    {
        [Inject] private SignalBus _signalBus;
        
        protected override void OnDie()
        {
            _signalBus.Fire(new Signals.PlayerDestroy());
            if (gameObject)
            {
                
            }
        }
    }
}