﻿using UnityEngine;

namespace Managers
{
    public class BackgroundManager
    {
        private readonly SpriteRenderer _spriteRenderer;

        public BackgroundManager
        (
            SpriteRenderer spriteRenderer
        )
        {
            _spriteRenderer = spriteRenderer;
        }

        public void SetBackground(Sprite sprite)
        {
            _spriteRenderer.sprite = sprite;
        }
    }
}