﻿using Managers;
using UnityEngine;

namespace Ship.Base
{
    [RequireComponent(typeof(Rigidbody))]
    public class ShipMove : MonoBehaviour
    {
        protected float _currentSpeed;
        protected Rigidbody _rigidbody;

        public void SetSpeed(float currentSpeed)
        {
            _currentSpeed = currentSpeed;
        }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        protected void DeltaMove(Vector3 delta)
        {
            _rigidbody.MovePosition(transform.position + delta);
        }
    }
}