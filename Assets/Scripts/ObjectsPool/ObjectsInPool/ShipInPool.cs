﻿using UnityEngine;

namespace ObjectsPool
{
    public class ShipInPool : ObjectInPool
    {
        [SerializeField] private GameObject _meshes;
        [SerializeField] private Collider _collider;
        
        public override void Reset()
        {
            _collider.enabled = true;
            _meshes.SetActive(true);
        }
    }
}