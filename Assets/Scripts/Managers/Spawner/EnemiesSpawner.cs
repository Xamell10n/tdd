﻿using System.Collections.Generic;
using AI;
using Constants;
using EnemiesPositionsSetter;
using ObjectsPool;
using Ship.Enemy;
using Tables.Ships;
using UnityEngine;
using Zenject;

namespace Managers.Spawner
{
    public class EnemiesSpawner : ShipSpawner
    {
        private readonly ShipsParameters _shipsParameters;
        private readonly EnemiesMover _enemiesMover;
        private readonly DiContainer _diContainer;
        private readonly Transform _parent;
        

        public EnemiesSpawner
        (
            ShipsParameters shipsParameters,
            Transform parent,
            EnemiesMover enemiesMover,
            DiContainer diContainer
        )
        {
            _shipsParameters = shipsParameters;
            _diContainer = diContainer;
            _parent = parent;
            _enemiesMover = enemiesMover;
        }

        public void Spawn(List<EnemyPosition> positions)
        {
            var prefabs = new Dictionary<string, GameObject>();
            foreach (var position in positions)
            {
                if (!prefabs.ContainsKey(position.Type))
                {
                    var prefab = Resources.Load<GameObject>("Ships/Enemies/" + position.Type);
                    prefabs[position.Type] = prefab;
                }
                var go = ObjectsPoolManager.Instantiate(prefabs[position.Type], position.Position);
                go.GetComponent<SavePosition>().enabled = false;
                _diContainer.InjectGameObject(go);
                go.transform.SetParent(_parent, false);
                go.tag = Tags.ENEMY;
                var baseParameters = _shipsParameters.BaseShipsStats[Tags.ENEMY];
                var multipliers = _shipsParameters.ShipsStatsMultipliers[position.Type];
                SetParameters(go, baseParameters, multipliers);
                _enemiesMover.Add(go.GetComponent<EnemyMove>());
            }
            _enemiesMover.Start();
        }
    }
}