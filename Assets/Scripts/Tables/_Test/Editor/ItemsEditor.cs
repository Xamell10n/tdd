﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;
using Utils.TablesDownloader;

namespace Tables._Test.Editor
{
    [CustomEditor(typeof(Items))]
    public class ItemsEditor : SpreadsheetDownloader
    {
        private JsonItems _jsonItems;
        private string _downloadingState = "";

        protected override string Sheet
        {
            get { return "Items"; }
        }

        public override void OnInspectorGUI()
        {
            if (DownLoading)
            {
                GUILayout.Label("Downloading " + _downloadingState, EditorStyles.boldLabel);
                return;
            }

            if (GUILayout.Button("Load Items") && !DownLoading)
            {
                DownLoading = true;
                _downloadingState = "1/1. Items...";
                _jsonItems = null;

                Load(Sheet);
            }
            base.OnInspectorGUI();
        }

        protected override void OnInitialDownLoad(string result)
        {
            result = new StringBuilder("{ \"List\":").Append(result).Append("}").ToString();
            _jsonItems = JsonUtility.FromJson<JsonItems>(result);
            _downloadingState = "Done!";
            Parse();
        }

        private void Parse()
        {
            DownLoading = false;
            var table = CurrentScriptableObject == null
                ? (Items) target
                : (Items) CurrentScriptableObject;
            var items = new List<Item>();
            table.Clear();
            foreach (var json in _jsonItems.List)
            {
                if (string.IsNullOrEmpty(json.Value))
                {
                    continue;
                }
                var item = new Item
                {
                    Index = json.Index,
                    Value = json.Value
                };
                items.Add(item);
            }
            table.ItemsArray = items.ToArray();
            Save();
        }

        [Serializable]
        private class JsonItems
        {
            public JsonItem[] List;
        }

        [Serializable]
        private class JsonItem
        {
            public int Index;
            public string Value;
        }
    }
}