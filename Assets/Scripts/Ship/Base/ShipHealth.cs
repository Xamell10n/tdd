﻿using Constants;
using ObjectsPool;
using UnityEngine;
using Zenject;

namespace Ship.Base
{
    [RequireComponent(typeof(Animator)), RequireComponent(typeof(AudioSource))]
    public abstract class ShipHealth : MonoBehaviour
    {
        private Animator _animator;
        private AudioSource _audioSource;
    
        private float _health;
        private float _maxHealth;
        private float _mobility;
        private float _weaponDamageMultiplier;
        protected float Score;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _audioSource = GetComponent<AudioSource>();
        }

        public void SetParameters(float health, float mobility, float weaponDamage, float score)
        {
            _maxHealth = health;
            _mobility = mobility;
            _weaponDamageMultiplier = weaponDamage;
            Score = score;
        
            _health = _maxHealth;
        }

        public bool Hit(float damage)
        {
            var percent = 100 - _mobility;
            var chance = Random.Range(0f, 100f);
            if (chance > percent)
            {
                NoDamage();
                return false;
            }
            Damage(damage);
            return true;
        }

        public void Damage(float damage)
        {
            var currentDamage = damage * _weaponDamageMultiplier;
            Debug.Log(gameObject.name + " Damage " + currentDamage);
            _health -= currentDamage;
            Debug.Log(gameObject.name + " HP remains " + _health);
            if (_health <= 0)
            {
                _health = 0;
                Debug.Log(gameObject.name + " Die!");
                OnDie();
                _animator.SetTrigger(Animations.Die);
                _audioSource.Play();
            }
        }

        public void Heal(float value)
        {
            Debug.Log(gameObject.name + " Heal " + value);
            _health += value;
            if (_health > _maxHealth)
            {
                _health = _maxHealth;
            }
        }

        protected abstract void OnDie();

        // uses in Animation
        public void OnAnimationDie()
        {
            ObjectsPoolManager.Destroy(gameObject);
        }

        private void NoDamage()
        {
        
        }
    }
}
