﻿using WindowSystem.Interfaces;
using Zenject;

namespace WindowSystem.Managers
{
    public class WindowsFabric
    {
        private readonly DiContainer _container;
        
        public WindowsFabric
        (
            DiContainer container
        )
        {
            _container = container;
        }

        public T Window<T>() where T : IWindowManager
        {
            return _container.Resolve<T>();
        }
    }
}