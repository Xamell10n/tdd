﻿using Managers;
using Tables.PlayerShips;
using WindowSystem.Managers;

namespace UI.Windows
{
    public class ShipPickWithArrowsManager : AbstractWindowManager<ShipPickWithArrowsView>
    {
        private readonly WindowsManager _windowsManager;
        private readonly PlayerShips _playerShips;
        private readonly PlayerManager _playerManager;

        private int _currentShip;

        public ShipPickWithArrowsManager
        (
            ShipPickWithArrowsView view,
            WindowsManager windowsManager,
            PlayerManager playerManager,
            PlayerShips playerShips
        ) : base(view)
        {
            _windowsManager = windowsManager;
            _playerShips = playerShips;
            _playerManager = playerManager;
        }
        
        public override void Listen()
        {
            View.Listen(OnStart, OnBack, OnNext, OnPrev);
            Refresh();
        }

        private void OnStart()
        {
            _playerManager.CurrentShip = _currentShip;
            SceneManager.Load(1);
        }

        private void OnBack()
        {
            _windowsManager.CloseWindow();
        }

        private void OnNext()
        {
            if (++_currentShip >= _playerShips.GetCount())
            {
                _currentShip = 0;
            }
            Refresh();
        }

        private void OnPrev()
        {
            if (--_currentShip < 0)
            {
                _currentShip = _playerShips.GetCount() - 1;
            }
            Refresh();
        }

        private void Refresh()
        {
            View.SetShip(_playerShips.GetSprite(_currentShip));

        }
    }
}