﻿using UnityEngine;

namespace UI.Components
{
    public class SoundButton : Button
    {
        private AudioSource _audioSource;
        
        private void Awake()
        {
            _audioSource = transform.root.GetComponent<AudioSource>();
        }

        protected override void Onclick()
        {
            _audioSource.Play();
        }
    }
}