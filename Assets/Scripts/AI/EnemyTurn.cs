﻿using Constants;
using Managers;
using UnityEngine;
using Zenject;

namespace AI
{
    [RequireComponent(typeof(Collider))]
    public class EnemyTurn : MonoBehaviour
    {
        [SerializeField] private bool _isMoveRight;

        private SignalBus _signalBus;

        [Inject]
        private void Initialize
        (
            SignalBus signalBus
        )
        {
            _signalBus = signalBus;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tags.ENEMY))
            {
                _signalBus.Fire(new Signals.EnemyTurn {IsMoveRight = _isMoveRight});
            }
        }
    }
}