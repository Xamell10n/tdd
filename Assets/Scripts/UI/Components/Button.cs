﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Components
{
    public abstract class Button : MonoBehaviour, IPointerClickHandler
    {
        private Action _callback;

        public void Listen(Action callback)
        {
            _callback = callback;
        }

        public void Unlisten()
        {
            _callback = null;
        }

        protected abstract void Onclick();

        public void OnPointerClick(PointerEventData eventData)
        {
            Onclick();
            if (_callback != null)
                _callback();
        }
    }
}