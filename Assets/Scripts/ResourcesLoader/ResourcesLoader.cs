﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

    public class ResourcesLoader
    {
        public static Link Init()
        {
            return new Link();
        }
        
        public class Link
        {
            private int _count;
            private Action _onLoadAll;
        
            public Link Add<T>(string path, out T asset, Action onLoad = null) where T : Object
            {
                Debug.Log("Ready to Load " + path);
                _count++;
                var request = Resources.LoadAsync<T>(path);
                asset = (T) request.asset;
                request.completed += x => OnLoad(x, onLoad);
                return this;
            }

            public Link Add<T>(string path, Action<T> onLoad) where T: Object
            {
                Debug.Log("Ready to Load " + path);
                _count++;
                var request = Resources.LoadAsync<T>(path);
                var asset = (T) request.asset;
                request.completed += x => OnLoad(x, () => onLoad(asset));
                return this;
            }

            private void OnLoad(AsyncOperation operation, Action onLoad)
            {
                Debug.Log("Load!");

                if (onLoad != null)
                {
                    onLoad.Invoke();
                }
                _count--;
                if (_count == 0)
                {
                    Debug.Log("Load All!");

                    if (_onLoadAll != null)
                    {
                        _onLoadAll.Invoke();
                        _onLoadAll = null;
                    }
                }
            }

            public void Load(Action onLoadAll = null)
            {
                _onLoadAll = onLoadAll;
            }
        }
    }
