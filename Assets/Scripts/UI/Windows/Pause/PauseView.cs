﻿using System;
using UI.Components;
using UnityEngine;
using WindowSystem.Views;

namespace CustomGUI.Windows
{
    public class PauseView : AbstractWindowView
    {
        [SerializeField] private Button _continue;
        [SerializeField] private Button _restart;
        [SerializeField] private Button _exit;
        
        public void Listen(Action onContinue, Action onRestart, Action onExit)
        {
            _continue.Listen(onContinue);
            _restart.Listen(onRestart);
            _exit.Listen(onExit);
        }
        
        public override void Unlisten()
        {
            _continue.Unlisten();
            _restart.Unlisten();
            _exit.Unlisten();
  
        }
    }
}