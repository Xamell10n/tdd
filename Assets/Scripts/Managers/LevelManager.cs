﻿using Managers.Spawner;
using Tables.Levels;
using UnityEngine;

namespace Managers
{
    public class LevelManager
    {
        public int MaxEnemies { get; private set; }

        private readonly LevelsParameters _levelsParameters;
        private readonly AudioManager _audioManager;
        private readonly BackgroundManager _backgroundManager;
        private readonly EnemiesSpawner _enemiesSpawner;
        private readonly PlayerSpawner _playerSpawner;
        private readonly PlayerManager _playerManager;

        public LevelManager
        (
            LevelsParameters levelsParameters,
            BackgroundManager backgroundManager,
            EnemiesSpawner enemiesSpawner,
            PlayerSpawner playerSpawner,
            PlayerManager playerManager,
            AudioManager audioManager
        )
        {
            _levelsParameters = levelsParameters;
            _audioManager = audioManager;
            _backgroundManager = backgroundManager;
            _enemiesSpawner = enemiesSpawner;
            _playerSpawner = playerSpawner;
            _playerManager = playerManager;
        }

        public void Initialize()
        {
            var parameters = _levelsParameters.GetParameters(_playerManager.CurrentLevel);
//            var audio = Resources.Load<AudioClip>("Audio/" + parameters.AudioBackgroundName);
//            _audioManager.PlayBackground(audio);
//            var sprite = Resources.Load<Sprite>("Sprites/" + parameters.SpriteBackgroundName);
//            _backgroundManager.SetBackground(sprite);
            ResourcesLoader.Init().Add<AudioClip>("Audio/" + parameters.AudioBackgroundName,
                _audioManager.PlayBackground).Add<Sprite>(
                "Sprites/" + parameters.SpriteBackgroundName, _backgroundManager.SetBackground);
            var positions = parameters.EnemiesPositions.Positions;
            MaxEnemies = positions.Count;
            _enemiesSpawner.Spawn(positions);
            _playerSpawner.Spawn();
        }

        public void LevelComplete(out bool isLastLevel)
        {
            isLastLevel = ++_playerManager.CurrentLevel >= _levelsParameters.Count();
        }
    }
}