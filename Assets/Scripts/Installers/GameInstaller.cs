﻿using AI;
using Bonuses;
using CustomGUI.Windows;
using Managers;
using Managers.Spawner;
using States;
using Tables.Bonuses;
using Tables.Levels;
using Tables.Ships;
using UI.Windows.LevelComplete;
using UnityEngine;
using WeaponSystem;
using Zenject;
using PauseManager = Managers.PauseManager;

namespace Installers
{
    public class GameInstaller : BaseMonoInstaller
    {
        [Header("Windows")]
        [SerializeField] private HUDView _hud;
        [SerializeField] private PauseView _pause;
        [SerializeField] private LevelCompleteView _levelComplete;

        [Header("Tables")]
        [SerializeField] private LevelsParameters _levelsParameters;
        [SerializeField] private ShipsParameters _shipsParameters;
        [SerializeField] private BonusesParameters _bonusesParameters;
        
        [Header("Ohter")]
        [SerializeField] private GameObject _background;
        
        protected override void DeclareSignals()
        {
            SignalBusInstaller.Install(Container);
            
            Container.DeclareSignal<Signals.EnemyDestroy>();
            Container.DeclareSignal<Signals.PlayerDestroy>();
            Container.DeclareSignal<Signals.EnemyTurn>();
            Container.DeclareSignal<Signals.EnemyFreeze>();
        }

        protected override void BindManagers()
        {
            Container.BindInterfacesAndSelfTo<GameManager>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<HUDManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<LevelManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<AudioManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<BackgroundManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<EnemiesSpawner>().AsSingle();
            Container.BindInterfacesAndSelfTo<PlayerSpawner>().AsSingle();
            Container.BindInterfacesAndSelfTo<PauseManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<CustomGUI.Windows.PauseManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<StatesManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<LevelCompleteManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<BonusesManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<BonusSpawner>().AsSingle();
            Container.BindInterfacesAndSelfTo<EnemiesMover>().AsSingle();
        }

        protected override void BindPrefabs()
        {
            BindWindow(_hud);
            BindWindow(_pause);
            BindWindow(_levelComplete);
            Container.BindInstance(_levelsParameters).AsSingle();
            Container.BindInstance(_shipsParameters).AsSingle();
            Container.BindInstance(_bonusesParameters).AsSingle();
        }

        protected override void BindOther()
        {
            var poolTransform = Container.CreateEmptyGameObject("PoolObjects").transform;
            var enemiesTransform = Container.CreateEmptyGameObject("Enemies").transform;
            enemiesTransform.SetParent(poolTransform);
            Container.BindInstance(enemiesTransform).WhenInjectedInto<EnemiesSpawner>();
            Container.BindInstance(poolTransform).WhenInjectedInto<PlayerSpawner>();
            var bonusesTransform = Container.CreateEmptyGameObject("Bonuses").transform;
            bonusesTransform.SetParent(poolTransform);
            Container.BindInstance(bonusesTransform).WhenInjectedInto<BonusSpawner>();
            var projectilesTransform = Container.CreateEmptyGameObject("Projectiles").transform;
            projectilesTransform.SetParent(poolTransform);
            Container.BindInstance(projectilesTransform).WhenInjectedInto<ProjectileWeapon>();
            var backgound = Container.InstantiatePrefab(_background);
            Container.BindInstance(backgound.GetComponent<AudioSource>())
                .WhenInjectedInto<AudioManager>();
            Container.BindInstance(backgound.GetComponent<SpriteRenderer>())
                .WhenInjectedInto<BackgroundManager>();
            poolTransform.position = Vector3.up;
        }
    }
}