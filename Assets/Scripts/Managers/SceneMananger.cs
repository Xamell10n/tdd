﻿using ObjectsPool;
using States;

namespace Managers
{
    public class SceneManager
    {
        public static void Load(int index)
        {
            ObjectsPoolManager.Clear();
            UnityEngine.SceneManagement.SceneManager.LoadScene(index);
            StatesManager.ChangeState(GameState.Game);
        }
        
        public static void LoadAsync(int index)
        {
            ObjectsPoolManager.Clear();
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(index);
            StatesManager.ChangeState(GameState.Game);
        }
    }
}