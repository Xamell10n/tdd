﻿using System;
using UnityEngine;
using UnityEngine.UI;
using WindowSystem.Views;
using Button = UI.Components.Button;

namespace UI.Windows
{
    public class ShipPickWithArrowsView : AbstractWindowView
    {
        [SerializeField] private Button _nextShip;
        [SerializeField] private Button _prevShip;
        [SerializeField] private Button _start;
        [SerializeField] private Button _back;
        [SerializeField] private Image _ship;
        
        public override void Unlisten()
        {
            _start.Unlisten();
            _back.Unlisten();
            _nextShip.Unlisten();
            _prevShip.Unlisten();
        }

        public void Listen(Action onStart, Action onBack, Action onNext, Action onPrev)
        {
            _start.Listen(onStart);
            _back.Listen(onBack);
            _nextShip.Listen(onNext);
            _prevShip.Listen(onPrev);
        }

        public void SetShip(Sprite sprite)
        {
            _ship.sprite = sprite;
        }
    }
}