﻿using System.Collections.Generic;
using UnityEngine;

namespace ObjectsPool
{
    public class ObjectsPoolManager
    {
        private static readonly Dictionary<GameObject, List<GameObject>> _objects =
            new Dictionary<GameObject, List<GameObject>>();
        
        public static GameObject Instantiate(GameObject prefab, Vector3 position)
        {
            GameObject gameObject = null;
            if (_objects.ContainsKey(prefab))
            {
                foreach (var @object in _objects[prefab])
                {
                    if (@object.activeInHierarchy) continue;
                    gameObject = @object;
                    gameObject.SetActive(true);
                    gameObject.transform.position = position;
                    break;
                }
                if (gameObject == null)
                {
                    gameObject = GameObject.Instantiate(prefab, position, Quaternion.identity);
                    _objects[prefab].Add(gameObject);
                }
            }
            else
            {
                gameObject = GameObject.Instantiate(prefab, position, Quaternion.identity);
                _objects[prefab] = new List<GameObject> {gameObject};
            }
            return gameObject;
        }

        public static GameObject Instantiate(GameObject prefab, Vector3 position, Transform parent)
        {
            var gameObject = Instantiate(prefab, position);
            gameObject.transform.SetParent(parent);
            return gameObject;
        }

        public static void Destroy(GameObject gameObject)
        {
            var objectInPool = gameObject.GetComponent<ObjectInPool>();
            if (objectInPool)
            {
                objectInPool.Reset();
            }
            gameObject.SetActive(false);
        }

        public static void Clear()
        {
            _objects.Clear();
        }
    }
}