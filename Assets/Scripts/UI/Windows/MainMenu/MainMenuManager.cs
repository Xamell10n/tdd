﻿#if UNITY_EDITOR
using UnityEditor;
#else
using UnityEngine;
#endif
using WindowSystem.Managers;

namespace UI.Windows
{
    public class MainMenuManager : AbstractWindowManager<MainMenuView>
    {
        private readonly WindowsManager _windowsManager;

        public MainMenuManager
        (
            MainMenuView view,
            WindowsManager windowsManager
        ) : base(view)
        {
            _windowsManager = windowsManager;
        }

        public override void Listen()
        {
            View.Listen(OnStart, OnExit);
        }

        private void OnStart()
        {
//            _windowsManager.OpenWindow<ShipPickManager>();
            _windowsManager.OpenWindow<ShipPickWithArrowsManager>();
        }

        private void OnExit()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}