﻿using System;
using UnityEngine;

namespace Tables.Ships
{

	[Serializable]
	public class BaseShipsStats
	{
		[Tooltip("HP"), Range(1, 1000)] public float Health;
		[Tooltip("Speed, units/s"), Range(0, 100)] public float MoveSpeed;
		[Tooltip("Shooting period"), Range(0, 100)] public float ShootSpeed;
		[Tooltip("Probability to avoid hit, %"), Range(0, 100)] public float Mobility;
		[Tooltip("Score"), Range(0, 100)] public int Score;
	}

}