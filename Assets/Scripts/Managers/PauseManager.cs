﻿using States;
using UnityEngine;
using WindowSystem.Managers;
using Zenject;

namespace Managers
{
    public class PauseManager : ITickable
    {
        private readonly WindowsManager _windowsManager;

        public static bool IsPause { get; private set; }
        private static float _prevTime;

        public PauseManager
        (
            WindowsManager windowsManager
        )
        {
            _windowsManager = windowsManager;
        }

        public static void SetPause(bool isPause)
        {
            if (IsPause == isPause) return;
            if (isPause)
            {
                _prevTime = Time.timeScale;
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = _prevTime;
            }
            IsPause = isPause;
        }

        public void Tick()
        {
            if (IsPause) return;
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _windowsManager.OpenWindow<CustomGUI.Windows.PauseManager>();
            }
        }
    }
}