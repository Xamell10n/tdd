﻿using System;
using Managers;
using States;
using WindowSystem.Interfaces;
using WindowSystem.Managers;

namespace UI.Windows.LevelComplete
{
    public class LevelCompleteManager : AbstractWindowManager<LevelCompleteView>, IOpen<LevelCompleteManager.States>
    {
        private readonly PlayerManager _playerManager;

        public LevelCompleteManager
        (
            LevelCompleteView view,
            PlayerManager playerManager
        ) : base(view)
        {
            _playerManager = playerManager;
        }

        public override void Listen()
        {
            View.Listen(OnNext, OnExit, OnRestart);
        }

        public void Open(States data)
        {
            StatesManager.ChangeState(GameState.Menu);
            switch (data)
            {
                case States.Complete:
                    View.SetLevelComplete();
                    View.SetText(Constants.COMPLETE.ToUpperInvariant());
                    break;
                case States.CompleteLast:
                    View.SetLevelCompleteLast();
                    View.SetText(Constants.COMPLETE_LAST.ToUpperInvariant());
                    break;
                case States.Fail:
                    View.SetLevelFail();
                    View.SetText(Constants.FAIL.ToUpperInvariant());
                    break;
                default:
                    throw new ArgumentOutOfRangeException("data", data, null);
            }
            View.SetScore(_playerManager.Score);
        }

        private void OnNext()
        {
            SceneManager.Load(1);
        }

        private void OnRestart()
        {
            SceneManager.Load(1);
        }

        private void OnExit()
        {
            SceneManager.Load(0);
        }
        
        public enum States
        {
            Fail,
            Complete,
            CompleteLast
        }
    }
}