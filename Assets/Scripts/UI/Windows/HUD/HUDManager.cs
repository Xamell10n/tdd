﻿using Managers;
using WindowSystem.Managers;

namespace CustomGUI.Windows
{
    public class HUDManager : AbstractWindowManager<HUDView>
    {
        public HUDManager(HUDView view) : base(view)
        {
        }

        public override void Listen()
        {
            
        }

        public void UpdateScore(int score)
        {
            View.SetScore(score);
        }

        public void UpdateGoal(int current, int goal)
        {
            View.SetGoal(current, goal);
        }
    }
}