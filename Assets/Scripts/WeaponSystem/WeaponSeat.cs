﻿using UnityEngine;

namespace WeaponSystem
{
	public class WeaponSeat : MonoBehaviour
	{
		public string Name;
		public Weapon Weapon { get; private set; }

		public void SetWeapon(Weapon weapon)
		{
			Weapon = Instantiate(weapon, transform.position, Quaternion.identity);
		}
	}
}