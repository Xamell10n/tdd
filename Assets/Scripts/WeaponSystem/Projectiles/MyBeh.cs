﻿using System.Collections;
using UnityEngine;

namespace WeaponSystem.Projectiles
{
    public class MyBeh : MonoBehaviour
    {
        private Coroutine _coroutine;
        
        private void OnEnable()
        {
            _coroutine = StartCoroutine(DoJob(5f));
            StartCoroutine(EndJob());
        }

        private IEnumerator DoJob(float time)
        {
            Debug.Log("DoJob Start");
            yield return new WaitForSeconds(4f);
            Debug.Log("DoJob 4 sec last");
            yield return new WaitForSeconds(4f);
            Debug.Log("DoJob 8 sec last");
            yield return new WaitForSeconds(4f);
            Debug.Log("DoJob 12 sec last");
        }

        private IEnumerator EndJob()
        {
            Debug.Log("EndJob Start");
            yield return new WaitForSeconds(2f);
            Debug.Log("EndJob 2 sec last, try to finish DoJob");
            StopCoroutine("DoJob");
            yield return new WaitForSeconds(4f);
            Debug.Log("EndJob 6 sec last, try to finish DoJob");
            StopCoroutine(DoJob(1f));
            yield return new WaitForSeconds(4f);
            Debug.Log("EndJob 10 sec last, try to finish DoJob");
            StopCoroutine(_coroutine);
        }
    }
}