﻿using Ship.Base;
using UnityEngine;

namespace Bonuses
{
    public class DamagePermanent : Bonus
    {
        [SerializeField] private float _percent;
        
        protected override void Trigger(GameObject target)
        {
            var attack = target.GetComponent<ShipAttack>();
            if (attack)
            {
                attack.AddDamage(_percent);
                Destroy();
            }

        }
    }
}