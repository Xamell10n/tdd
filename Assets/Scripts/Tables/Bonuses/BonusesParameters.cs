﻿using System;
using System.Linq;
using Boo.Lang;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Tables.Bonuses
{
    [CreateAssetMenu(menuName = "Scriptable/BonusesParameters", fileName = "BonusesParameters")]
    public class BonusesParameters : ScriptableObject
    {
        [SerializeField] private float _speed;
        [SerializeField] private Bonuses _bonuses;

        private int[] _weights;
        private string[] _names;
        
        public float Speed {get { return _speed; }}

        public string GetRandomBonus()
        {
            var random = Random.Range(0, _weights.Last());
            for (var i = 0; i < _weights.Length; i++)
            {
                if (random < _weights[i])
                {
                    return _names[i];
                }
            }
            return global::Bonuses.Constants.NONE;
        }

        private void OnEnable()
        {
            Initialize();
        }

        private void Initialize()
        {
            var fullWeight = 0;
            var weights = new List<int>();
            var names = new List<string>();
            foreach (var bonus in _bonuses)
            {
                if (bonus.Value.Weight == 0)
                {
                    continue;
                }
                fullWeight += bonus.Value.Weight;
                weights.Add(fullWeight);
                names.Add(bonus.Key);
            }
            _names = names.ToArray();
            _weights = weights.ToArray();
        }
    }
    
    [Serializable]
    public class Bonuses : SerializableDictionaryBase<string, BonusParameters> { }
}