﻿using System.Collections.Generic;
using Constants;
using EnemiesPositionsSetter;
using ObjectsPool;
using Ship.Enemy;
using Tables.Bonuses;
using UnityEngine;
using Zenject;

namespace Bonuses
{
    public class BonusSpawner
    {
        private readonly Transform _parent;
        private readonly DiContainer _diContainer;
        
        private Dictionary<string, GameObject> _prefabs = new Dictionary<string, GameObject>();

        public BonusSpawner
        (
            Transform parent,
            DiContainer diContainer
        )
        {
            _parent = parent;
            _diContainer = diContainer;
        }

        public void Spawn(string bonusName, Vector3 position, float speed)
        {
            if (_prefabs.ContainsKey(bonusName))
            {
            }
            else
            {
                var prefab = Resources.Load<GameObject>("Bonuses/" + bonusName);
                _prefabs[bonusName] = prefab;
            }
            var go = ObjectsPoolManager.Instantiate(_prefabs[bonusName], position);
            _diContainer.InjectGameObjectForComponent<Bonus>(go);
            go.transform.SetParent(_parent);
            go.tag = Tags.BONUS;
            go.GetComponent<Bonus>().SetSpeed(speed);
        }
    }
}