﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Utils.TablesDownloader
{
    public class SpreadsheetConnection
    {
        private readonly SpreadsheetSettings _settings;
        private UnityWebRequest _request;

        public SpreadsheetConnection(SpreadsheetSettings settings)
        {
            _settings = settings;
        }

        public IEnumerator Create(Dictionary<string, string> form, Action<string> callback)
        {
            form.Add("ssid", _settings.Spreadsheet);
            form.Add("pass", _settings.Password);
            _request = UnityWebRequest.Post(_settings.Url, form);
            _request.SendWebRequest();
            var begin = Time.realtimeSinceStartup;
            while (!_request.isDone)
            {
                if (Time.realtimeSinceStartup - begin >= _settings.Timeout)
                {
//                    D.Log("Time out:", Time.realtimeSinceStartup - begin);
                    Debug.Log("Time out");
                    break;
                }
                yield return null;
            }
//            float elapsedTime = Time.realtimeSinceStartup - begin;
            if (!string.IsNullOrEmpty(_request.error))
            {
//                D.Log(
//                    "Connection error after " + elapsedTime.ToString(CultureInfo.InvariantCulture) +
//                    " seconds: " + _request.error,
//                    elapsedTime);
                Debug.Log("Connection error");
                yield break;
            }
            callback(_request.downloadHandler.text);
        }
    }
}