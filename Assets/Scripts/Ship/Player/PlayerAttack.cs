﻿using Constants;
using Ship.Base;
using UnityEngine;

namespace Ship.Player
{
    public class PlayerAttack : ShipAttack
    {
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Fire();
            }
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                NextWeapon();
            }
        }
    }
}