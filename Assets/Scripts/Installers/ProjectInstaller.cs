﻿using Managers;
using Zenject;

namespace Installers
{
    public class ProjectInstaller : BaseMonoInstaller
    {
        protected override void DeclareSignals()
        {
            
        }

        protected override void BindManagers()
        {
            Container.BindInterfacesAndSelfTo<PlayerManager>().AsSingle();
        }

        protected override void BindPrefabs()
        {
            
        }

        protected override void BindOther()
        {
            
        }
    }
}