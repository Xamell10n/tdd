﻿using UnityEngine;

namespace Utils.TablesDownloader
{
	public abstract class DownloadableScriptableObject : ScriptableObject
	{
		[SerializeField] private string _spreadSheetDownloader;

		public abstract void Clear();

		public string SpreadSheetDownloader
		{
			get { return _spreadSheetDownloader; }
		}
	}
}