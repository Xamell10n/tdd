﻿using System.Collections.Generic;
using Constants;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace EnemiesPositionsSetter
{
	[CustomEditor(typeof(EnemiesPositions))]
	public class EnemiesPositionsEditor : Editor
	{
		private ReorderableList _list;
		
		public override void OnInspectorGUI()
		{
			if (GUILayout.Button("Save Positions"))
			{
				SavePositions();
			}
			base.OnInspectorGUI();
		}

		private void SavePositions()
		{
			var enemies = GameObject.FindGameObjectsWithTag(Tags.ENEMY);
			var asset = (EnemiesPositions) target;
			var enemiesPositions = asset.Positions = new List<EnemyPosition>();
			for (var i = 0; i < enemies.Length; i++)
			{
				var savePos = enemies[i].GetComponent<SavePosition>();
				if (savePos == null)
				{
					Debug.LogErrorFormat("Enemy {0} has not component SavePosition",
						enemies[i].gameObject.name);
					continue;
				}
				enemiesPositions.Add(new EnemyPosition
				{
					Type = savePos.Type,
					Position = savePos.transform.position
				});
			}
			EditorUtility.SetDirty(asset);
		}
	}
}