﻿using Ship.Base;
using Tables.Ships;
using UnityEngine;

namespace Managers.Spawner
{
    public class ShipSpawner
    {
        protected void SetParameters(GameObject go, BaseShipsStats baseParameters,
            ShipsStatMultipliers multipliers)
        {
            SetHealth(go, baseParameters, multipliers);
            SetAttack(go, baseParameters, multipliers);
            SetMove(go, baseParameters, multipliers);
        }
        
        private void SetHealth(GameObject go, BaseShipsStats baseParameters,
            ShipsStatMultipliers multipliers)
        {
            var health = baseParameters.Health * multipliers.Health;
            var mobility = baseParameters.Mobility * multipliers.Mobility;
            var damage = multipliers.WeaponDamage;
            var score = baseParameters.Score * multipliers.Score;
            go.GetComponent<ShipHealth>().SetParameters(health, mobility, damage, score);
        }

        private void SetAttack(GameObject go, BaseShipsStats baseParameters,
            ShipsStatMultipliers multipliers)
        {
            var shootSpeed = baseParameters.ShootSpeed * multipliers.ShootSpeed;
            go.GetComponent<ShipAttack>().SetShootSpeed(shootSpeed);
        }

        private void SetMove(GameObject go, BaseShipsStats baseParameters,
            ShipsStatMultipliers multipliers)
        {
            var speed = baseParameters.MoveSpeed * multipliers.MoveSpeed;
            go.GetComponent<ShipMove>().SetSpeed(speed);
        }

    }
}