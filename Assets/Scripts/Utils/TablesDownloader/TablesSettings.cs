﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Utils.TablesDownloader
{
	[CreateAssetMenu(menuName = "Scriptable/Other/TablesSettings", fileName = "TablesDownloadSettings")]
	public class TablesSettings : ScriptableObjectInstaller<TablesSettings>
	{
		public bool IsParallel;
		public List<TableSettings> Settings;

		[Serializable]
		public class TableSettings
		{
			public bool IsDownload;
			public DownloadableScriptableObject Table;
		}
	}
	
}