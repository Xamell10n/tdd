﻿using System;
using UnityEngine;

namespace EnemiesPositionsSetter
{
    [Serializable]
    public class EnemyPosition
    {
        public string Type;
        public Vector3 Position;
    }
}